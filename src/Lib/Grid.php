<?php
/*
 * 表格元素 https://element.eleme.cn/#/zh-CN/component/table
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-22 16:42:42
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-27 00:29:37
 */

namespace Ldy\Lib;

use Ldy\Model;
use Ldy\Traits\Grid\Column;
use Ldy\Traits\Grid\Search;
use Ldy\Traits\Grid\Config;
use Ldy\Grid\Model as GridModel;

class Grid{

    use Column;
    use Search;
    use Config;

    protected $gridModel;

    /**
     * 搜索表单元素
     *
     * @var Array
     */
    protected $queryElement = [];

    protected $query = [];

    /**
     * 列表数据
     *
     * @var array
     */
    protected $list = [];

    protected $request = [];

    protected $param = [];
    
    /**
     * true 第二次请求列表，不再统计数据条数
     *
     * @var boolean
     */
    private $_not_first = false;

    private $_is_query = false;

    private $joinTableParam = [];

    protected $manualAlias = false;//手工设置了别名

    public function __construct(Model $model)
    {
        $this->gridModel = new GridModel($model, $this);
        $this->request = request();
        $this->param = $this->request->get();

        $this->_not_first = isset($this->param['not_first']) && !empty($this->param['not_first']) ? true:false;

        $this->_is_query = isset($this->param['is_query']) && !empty($this->param['is_query']) ? true:false;
    }

    public function model(){
        return $this->gridModel;
    }

    public function get(){

        $list =  $this->model()->get();

        $page = $this->model()->getPage();

        $column = $this->model()->getColumn();

        $res = [
            "column"=>$column, 
            "query_element"=>$this->queryElement,
            "query_element_num" => $this->rowElementNum,
            "query_element_width" => $this->queryElemenWidth,
            "table"=>$this->table,
            "list" => $list
        ];

        if(!$this->outTree) $res["page"] = $page;
        
        //不是首次请求，只返回数据列
        if($this->_not_first) $res = ["list"=>$list];

        if($this->_is_query) $res['page'] = $page;

        return $res;
    }

    public function getDataList(){
        $list = $this->model()->get();
        return $list;
    }

    public function getQueryElement(){
        return $this->queryElement;
    }
    /**
     * Undocumented function
     * 联表 使用方法就是拼接 thinkorm 的 模型操作方法
     * @param [type] $table ip_video v
     * @param [type] $field
     * @param string $type
     * @return $this
     */
    public function joinTable($table, $field, $type="leftJoin"){
        $this->joinTableParam[] = ["type"=>$type, "param"=>[$table, $field]];
        return $this;
    }

    /**
     * 设置主表别名
     *
     * @param [type] $alias
     * @return $this
     */
    public function alias($alias){
        $this->manualAlias = true;
        $this->model()->alias($alias);
        return $this;
    }

    public function __call($method, $arguments)
    {
        call_user_func_array([$this->gridModel, $method], $arguments);
        return $this;
    }

    public function __get($name)
    {
        return isset($this->$name) ? $this->$name:null;
    }
}