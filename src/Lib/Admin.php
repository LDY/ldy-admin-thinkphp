<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-12 15:01:45
 */

namespace Ldy\Lib;

use think\facade\Route;
use Ldy\Controller;

class Admin
{
    /**
     * 版本号
    * @var string
    */
    const version = '1.0.0';


    public function route(){
        $config = config('admin.route');

        //对应webman
       $this->routeRest();

        Route::group( '/'.$config['prefix'], function(){
        
            // Route::get('//', "Ldy\\Controller\\Index@index");

            //登录接口
            Route::post('/token', 'Ldy\Controller\Account::login')->name('login');
            //退出
            Route::get('/logout', 'Ldy\Controller\Account::logout')->name('logout');
            //获取账户菜单
            Route::get('/system/menu/my', 'Ldy\Controller\Account::myMenu')->name('myMenu');
            // //获取菜单列
            Route::get('/system/menu/list', 'Ldy\Controller\System::menuList')->name('menuList'); 
            
            // //文件上传接口后台的
            Route::post('/files/upload', 'Ldy\Controller\BaseAdmin::upload');
            // 上传头像
            Route::post('/avatar/upload', 'Ldy\Controller\Account::avatarUpload');
			
            // //系统设置
			Route::resource('/system/setting', 'Ldy\Controller\Setting')->only(['create','save']);

            // //模块管理
            Route::resource('/system/modules', 'Ldy\Controller\Modules');

            // //部门管理
            Route::resource('/system/auth/department', 'Ldy\Controller\Department');

            // //API管理
            Route::resource('/system/auth/api_manage', 'Ldy\Controller\ApiManage');

            // //菜单管理
            Route::resource('/system/auth/menu', 'Ldy\Controller\Menu');

            //  //角色管理
            Route::resource('/system/auth/role', 'Ldy\Controller\RoleManage');

            // //用户关联
            Route::resource('/system/auth/admin', 'Ldy\Controller\AdminManage');

            // //获取数据表字段信息
            // Route::get('/system/get_table_fields', "Ldy\\Controller\\System@getTableFields");

            // //账号信息
            Route::get('/system/account_info', 'Ldy\Controller\Account::accountInfo');
            // //账号信息修改
            Route::post('/system/account_info', 'Ldy\Controller\Account::accountInfo');
            // //密码修改
            Route::post('/system/update_password','Ldy\Controller\Account::updatePassword');
            //重置密码
            Route::get('/system/resetting_password', 'Ldy\Controller\Account::resettingPass');

            // //系统操作日志
            Route::get('/system/logs', 'Ldy\Controller\Logs::index');
            Route::get('/system/logs/<id>', 'Ldy\Controller\Logs::show');
            

            Route::get('/system/msg', 'Ldy\Controller\Message::index')->name('msg');
            
            Route::get('/system/long_task/restore/<id>','Ldy\Controller\BackupData::restore');
            Route::post('/system/long_task/backup','Ldy\Controller\BackupData::store');
            Route::resource('/system/backup','Ldy\Controller\BackupData')->only(["index","create","read","delete"]);
            

        })->middleware([
            \Ldy\Middleware\Logs::class,
            \Ldy\Middleware\AccessControl::class
        ]);
        

        // Route::miss(function(){
        //     return json(['code' => 404, 'msg' => '404 not found']);
        // });
    }

    public function routeRest(){
        Route::rest([
            'save'   => ['POST', '', 'store'],
            'read' => ['GET', '/:id', 'show'],
            'delete' => ['DELETE', '/:id', 'destroy']
        ]);
    }


    public function checkAccess(string $controller, string $action, int &$code = 0, string &$msg = ''){
        // 获取控制器鉴权信息
        $class = new \ReflectionClass($controller);
        $properties = $class->getDefaultProperties();
        $noNeedLogin = $properties['noNeedLogin'] ?? [];
        $noNeedAuth = $properties['noNeedAuth'] ?? [];

        // 不需要登录
        if (in_array($action, $noNeedLogin)) {
            return true;
        }

        // 获取登录信息
        $admin = admin_info();
        if (!$admin) {
            $msg = '请登录';
            // 401是未登录固定的返回码
            $code = 401;
            return false;
        }

        // 不需要鉴权
        if (in_array($action, $noNeedAuth)) {
            return true;
        }

        // 当前管理员无角色
        $roles = $admin['roles'];
        if (empty($roles)) {
            $msg = '无权限';
            $code = 2;
            return false;
        }
    }
}