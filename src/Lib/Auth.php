<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-12 15:01:45
 */

namespace Ldy\Lib;

use tauthz\facade\Enforcer as Permission;
use think\Request;

class Auth{

    /**
     * 验证账号权限
     *
     * @param string $controller
     * @param string $action
     * @param integer $code
     * @param string $msg
     * @return void
     */
    public static function checkAccount(Request $request, int &$code = 0, string &$msg = ''){

        $route = get_controller_action($request->rule()->getRoute());

        $controller = $route['controller'];
        $action = $route['action'];
        $method = $request->method();
        $path = $request->baseUrl();
        $path = str_replace('/'.admin_route_prefix(),'', $path);
        // print_r($method.'---');
        // print_r(admin_id().'---');
// print_r($path);
        //超级管理员直接通过
        if(is_root()) return true;

        // 获取控制器鉴权信息
        $class = new \ReflectionClass($controller);
        $properties = $class->getDefaultProperties();
        $noNeedLogin = $properties['noNeedLogin'] ?? [];
        $noNeedAuth = $properties['noNeedAuth'] ?? [];

        // 不需要登录
        if (in_array($action, $noNeedLogin)) {
            return true;
        }

        // 获取登录信息
        $admin = admin_info();
        
        if (!$admin || !check_token($request)) {
            $msg = '当前用户已被登出或未登陆，请尝试重新登录后再操作。';
            // 401是未登录固定的返回码
            $code = 401;
            return false;
        }

        // 不需要鉴权
        if (in_array($action, $noNeedAuth)) {
            return true;
        }

        // 当前管理员无角色
        $roles = $admin['roles'];
        if (empty($roles)) {
            $msg = '当前用户未分配到角色或无权限访问当前资源，请联系管理人员！';
            $code = 404;
            return false;
        }

        $admin_id = strval(admin_id());
        
        if (!Permission::enforce($admin_id, $path, $method)){
            $msg = '当前用户无权限访问当前资源，请联系管理人员！';
            $code = 404;
            return false;
        }

        return true;
    }
}