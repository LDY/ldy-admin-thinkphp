<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-13 12:35:01
 */

use think\facade\Log;

if (!function_exists('admin')) {

    function admin()
    {
        return \Ldy\Facade\Admin::class;
    }
}

if (!function_exists('casbin_role_field_prefix')) {

    function casbin_role_field_prefix($role_id = 0)
    {
        $prefix = config('admin.casbin_role_field_prefix');
        $res = $role_id ? $prefix . $role_id : $prefix;
        return $res;
    }
}

if (!function_exists('password_encode')) {
    function password_encode($pass, $algo = PASSWORD_DEFAULT)
    {
        $secretKey = 'lc_qs_com@admin';
        return password_hash(md5($pass) . md5($secretKey), $algo);
    }
}

if (!function_exists('password_ver')) {
    function password_ver($input_pass, $db_pass)
    {
        $secretKey = 'lc_qs_com@admin';
        return password_verify($input_pass . md5($secretKey), $db_pass);
    }
}

/**
 * 当前登录管理员id
 *
 * @return mixed|null
 */
if (!function_exists('is_root')) {
    function is_root()
    {
        if (!$admin = session('admin')) return false;
        $root_name = config('admin.root_name');
        return $admin['name'] == $root_name;
    }
}

if (!function_exists('root_name')) {
    function root_name()
    {
        return config('admin.root_name') ?? 'admin';
    }
}

/**
 * 当前登录管理员id
 *
 * @return mixed|null
 */
if (!function_exists('admin_id')) {
    function admin_id()
    {
        if (!$admin = session('admin')) return null;
        return $admin['id'];
    }
}
/**
 * 当前管理员
 *
 * @param null|array|string $fields
 * @return array|mixed|null
 */
if (!function_exists('admin_info')) {

    function admin_info($fields = null)
    {
        if (!($admin = session('admin'))) {
            return null;
        }
        if ($fields === null) {
            return $admin;
        }
        if (is_array($fields)) {
            $results = [];
            foreach ($fields as $field) {
                $results[$field] = $admin[$field] ?? null;
            }
            return $results;
        }
        return $admin[$fields] ?? null;
    }
}

if (!function_exists('admin_route_prefix')) {
    function admin_route_prefix()
    {
        $route_prefix = config('admin.route.prefix');
        return $route_prefix ? $route_prefix : 'admin';
    }
}

/**
 * 生成 token
 */
if (!function_exists('get_token')) {
    function generate_token($request, $admin_info)
    {
        $userA = md5($request->header('User-Agent'));
        $res = md5($admin_info['id'] . $admin_info['name'] . $admin_info['last_login_ip'] . $userA);
        return $res;
    }
}

if (!function_exists('check_token')) {
    function check_token($request, $token = '')
    {
        if(empty($token)) $token = $request->cookie('TOKEN');
        if(empty($token)) $token = $request->header('TOKEN');

        $userA = md5($request->header('User-Agent'));
        $session = session('admin');
        $res = md5($session['id'] . $session['name'] . $session['last_login_ip'] . $userA);
        return $res === $token ? true : false;
    }
}


if (!function_exists('del_dir')) {
    /**
     * 删除文件夹
     *
     * @param [type] $dir
     * @return void
     */
    function del_dir($dir)
    {
        if (!file_exists($dir)) return false;

        if (is_dir($dir)) {
            $files = @scandir($dir);
            foreach ($files as $file) {
                if ($file != '.' && $file != '..') del_dir($dir . '/' . $file);
            }
            @rmdir($dir);
        } else {
            @unlink($dir);
        }

        return true;
    }
}

if (!function_exists('session_id_refresh')){
    /**
     * 刷新session id
     *
     * @param [type] $request 请求对象
     * @return void
     */
    function session_id_refresh($request){
        //刷新session_id
        // $session_id = \bin2hex(\pack('d', \microtime(true)) . random_bytes(8));
        // $request->sessionId($session_id);
        \think\facade\Session::regenerate(true);
    }
}

if(!function_exists('get_controller_action')){
    /**
     * 拆分路由
     * Ldy\Controller\Account::myMenu
     * 获取 控制器 和 方法
     *
     * @param [type] $route
     * @return void
     */
    function get_controller_action($route){
        $route = str_replace('::','@',$route);
        $route = str_replace('/','@',$route);
        [$controller, $action] = explode('@', $route);
        return ["controller"=>$controller, "action"=>$action];
    }
}

if(!function_exists('aes_encrypt')){

    /**
     * 加密函数
     */
    function aes_encrypt($data, $key, $isUrldecrypt = false){
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('AES-256-CBC'));
        $encrypt = openssl_encrypt($data, 'AES-256-CBC',$key,0, $iv);
        $base64 = base64_encode($iv.$encrypt);
        return $isUrldecrypt ? urlencode($base64):$base64;
    }
}

if(!function_exists('aes_decrypt')){

    /**
     * 解密
     *
     * @param [type] $data
     * @param [type] $key
     * @return String
     */
    function aes_decrypt($data, $key, $isUrldecrypt = false){
        if($isUrldecrypt) $data = urldecode($data);
        $data = base64_decode($data);
        $iv_len = openssl_cipher_iv_length('AES-256-CBC');
        $iv = substr($data,0, $iv_len);
        $encrypt = substr($data, $iv_len);
        $decrypt = openssl_decrypt($encrypt, 'AES-256-CBC',$key, 0 ,$iv);
        return $decrypt;
    }
}


if(!function_exists('rand_string')){

    /**
     * 随机字符串
     *
     * @param integer $len 随机字符串长度
     * @return string
     */
    function rand_string($len = 6):string{
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $randomString = '';
        $charLength = strlen($characters);
        
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charLength - 1)];
        }
        
        return $randomString;
    }
}