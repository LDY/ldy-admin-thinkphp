<?php
/*
 * @Date: 2022-10-12 21:15:43
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-19 13:39:13
 */

namespace Ldy\Lib;

class Show{
 
    protected $fields = [];

    protected $currFieldKey = '';

    protected $item = [];

    public function __construct($item = [])
    {
        if(!empty($item)) $this->item = is_array($item) ?$item:$item->toArray();
    }


     /**
     * 列表值前缀文本
     *
     * @param string $text
     * @return $this
     */
    public function prefixText(String $text){
        $this->__setField("display.prefix", $text);
        return $this;
    }

    /**
     * 列表值后缀文本
     *
     * @param String $text
     * @return $this
     */
    public function suffixText(String $text){
        $this->__setField("display.suffix", $text);
        return $this;
    }
    /**
     * 设置组件属性
     *
     * @param Array $attrs
     * @return $this
     */
    public function attrs(Array $attrs){
        $this->__setField('display.attrs', $attrs);
        return $this;
    }

    public function get(){
        $fields = [];
        foreach($this->fields as $key => $item) {
            if(!isset($item['hide'])) $fields[] = $item;
        }

        return $fields;
    }
    /**
     * 字段
     *
     * @param String $fieldName
     * @param String $title
     * @return $this
     */
    public function field(String $fieldName, String $label){
        $attr = ["label"=> $label, "prop" => $fieldName, "display"=>["type"=>"text"]];
        $this->__addField($fieldName, $attr);

        return $this;
    }

    public function avatar($size = 50, $url= ''){
        $this->__setFieldDisplay('avatar', ["size"=>$size, "url"=>$url]);
        return $this;
    }

     /**
     * 内容映射
     *
     * @param Array $arr 一维数组|二维数据
     * [1=>'开启',0=>'关闭']
     * [1=>['label'=>'开启','type'=>'success/info/warning/danger'], 0=>[...]]
     * @return $this
     */
    public function using(Array $arr){
        $this->__setFieldDisplay("using", $arr, false);
        return $this;
    }

    /**
     * 标签
     * 可以通过 attrs 方法设置组件的属性 详情参考 https://element.eleme.cn/#/zh-CN/component/tag
     * 
     * @param String|Array $val 设置组件类型 success/info/warning/danger， 可以是数组，key为数据的值
     * @param String $sign 数据拆分符号，多标签使用
     * @return $this
     */
    public function tag($val, String $sign = ''){
        $this->__setFieldDisplay("tag", $val);
        if(!empty($sign)) $this->__setField('display.sign', $sign);

        return $this;
    }

    /**
     * 添加字段
     *
     * @param String $fieldName
     * @param array $attr
     * @return void
     */
    protected function __addField(String $fieldName, Array $attr = []){
        
        $this->currFieldKey = $fieldName;

        $attr['value'] = $this->item[$fieldName] ?? null;

        $this->fields[$fieldName] = $attr;
    }
    
     /**
     * 设置列属性
     *
     * @param string $key
     * @param [type] $val
     * @return void
     */
    private function __setField(string $key, $val){
        $tmp = explode('.', $key);
        if(count($tmp) > 1 && isset($this->fields[$this->currFieldKey][$tmp[0]])){
            //支持 var.var 模式
            $this->fields[$this->currFieldKey][$tmp[0]][$tmp[1]] = $val;
        }else{
            $this->fields[$this->currFieldKey][$key] = $val;
        }
    }
    

    /**
     * 设置前端显示方式
     *
     * @param String $type
     * @param $data
     * @param Bloot $toOneArr 默认二维转一维
     * @return void
     */
    private function __setFieldDisplay(String $type, $data=[], $toOneArr = true){

        $res = ["type"=>$type];
        
        if(!empty($data)){
            $isTwoArr = false;
            if(is_array($data) && count($data) != count($data,1)) $isTwoArr = true;

            if($isTwoArr && $toOneArr){
                //二维转一维
                foreach($data as $key=>$arr) $res[$key] = $arr;
            }else{
                $res['data'] = $data;
            }
        }
        $this->__setField("display", $res);
    }
}