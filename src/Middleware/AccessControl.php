<?php
/*
 * @Date: 2022-09-27 11:23:42
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-11 10:02:31
 */
namespace Ldy\Middleware;

use Ldy\Lib\Auth;

class AccessControl{

    public function handle($request, \Closure $next)
    {
        $code = 0;
        $msg = '';
        if(!Auth::checkAccount($request, $code, $msg)){
            if($code === 401) session_id_refresh($request);
            return json(['code' => $code, 'msg' => $msg]);
        }
        return $next($request);
    }
}