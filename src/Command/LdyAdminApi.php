<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-15 15:18:51
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-21 20:26:34
 */

namespace Ldy\Command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Route;
use Ldy\Models\SysApi;

class LdyAdminApi extends Command{

    protected static $defaultName = 'ldy-admin:api';
    protected static $defaultDescription = '添加API接口到管理表';

    /**
     * @return void
     */
    protected function configure()
    {
        // $this->addArgument('name', InputArgument::REQUIRED, '输入控制器路径');

        $this->setName('ldy-admin:api')
        ->addArgument('name', Argument::REQUIRED, "输入控制器路径")
        ->addOption('pid', null, Option::VALUE_OPTIONAL,'父id')
        ->setDescription('创建API地址到数据库 参数--pid 父id');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(Input $input, Output $output)
    {
        $name = $input->getArgument('name');
        $pid = $input->getOption('pid');
// print_r($name);
        $this->app->route->clear();
        $this->app->route->lazy(false);

        $path = $this->app->getRootPath() . 'route' . DIRECTORY_SEPARATOR.'route.php';
        
// print_r($path);
        include $path;

        $routes = $this->app->route->getRuleList();

        // print_r($rows);
        $class = null;
        $routePrefix = config('admin.route.prefix');
        $text = ['index'=>'列表','create'=>'表单','show'=>'查看','store'=>'新增','update'=>'修改','destroy'=>'删除'];

        $parentId = 0;

        foreach($routes as $item){

            $route = $item['route'];
            $method = strtoupper($item['method']);
            $rule = $item['rule'];

            $arr = explode('::',$route);
            if(count($arr) < 2) $arr = explode('/', $route);

            [$controller, $act] = $arr;

            if(empty($controller) || $name != $controller || !isset($text[$act])) continue;

            // $output->writeln($controller.'=='.$act);

            $class = new $controller;
            $title = $class->getTitle();
            $path = str_replace([$routePrefix,'<','>'], ['','{','}'], $rule);

            // $output->writeln($path.'=='. $act.'=='.$controller);

            // [$act] = $obj->getMethods();

            $is = SysApi::where(['path'=>$path, 'action'=>$method])->count();

            if($is) {
                $output->writeln("已存在 [$method][$path]");
                continue;
            }

            if ($act == 'index'){
                $model = SysApi::create(['name'=>$title ? $title:"未命名", 'path'=>$path.'*','action'=>"ALL", 'parent_id'=>$pid ?:0]);
                $parentId = $model->id;
            }

            $item = ["name"=>$text[$act], 'path'=>$path,'action'=>$method, 'parent_id'=>$parentId];

            SysApi::create($item);

            $output->writeln("新增接口-{$title} [".$text[$act]."][$method][$path]");
        }
    }
}