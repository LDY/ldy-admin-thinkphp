<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-15 15:18:51
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-22 11:46:03
 */

namespace Ldy\Command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

use Ldy\Facade\ClientConfig;

class LdyAdminGenerateConfig extends Command{

    // protected static $defaultName = 'ldy-admin:clientConfig';
    // protected static $defaultDescription = '生成客户端配置文件';


    protected function configure()
    {
        $this->setName('ldy-admin:tocc')
        	// ->addArgument('clientConfig', Argument::OPTIONAL, "生成客户端配置")
            // ->addOption('city', null, Option::VALUE_REQUIRED, 'city name')
        	->setDescription('生成客户端配置');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(Input $input, Output $output)
    {
        
        ClientConfig::generateConfig();

        $output->writeln('生成前端配置文件成功!');
    }
}