<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-11 16:51:32
 */

use think\Route;

use Ldy\Facade\Admin;

foreach (glob(app_path() . '/*/route/*.php') as $filename) {
    include_once($filename);
}

Admin::route();
