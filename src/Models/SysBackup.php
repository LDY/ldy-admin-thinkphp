<?php
/*
 * @Date: 2024-03-26 14:04:58
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-04-24 22:56:58
 */

namespace Ldy\Models;

use Ldy\Model;
use think\facade\Db;

class SysBackup extends Model
{

    public $updateWhiteList = ["folder", "size", "status","sql_num"];

    public $status_array = [-1 => "备份中", 0 => "备份失败", 1 => "备份完成"];


    public function getTablesOption($exclude = [])
    {
        $tables = Db::getTables();
// print_r($tables);
        $res = [];
        $selected = [];
        foreach ($tables as $val) {
            if(in_array($val, $exclude)) continue;
            $res[] = ['label' => $val, 'value' => $val];
            $selected[] = $val;
        }

        return ['option' => $res, 'value' => $selected];
    }

    public function setTablesAttr($val)
    {
        if (is_array($val)) return implode(',', $val);
    }

    public function getTablesAttr($val)
    {
        if(empty($val) || is_array($val)) return $val;
        return explode(",", $val);
    }
}
