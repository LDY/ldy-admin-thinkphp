<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-13 08:46:06
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-08-19 13:41:11
 */

namespace Ldy\Models;

use Ldy\Model;

class SysModules extends Model{

    //设置字段名称
    public $fieldsLabel = [
		"id"=>["comment" =>"ID", "width"=>"80"],
		"desc"=>["width"=>"300"]
	];

	protected $rule = [
		"label"=>"require|min:2",
		"name" => "require|min:2"
	];

	protected $message = [
		"label.min" => "最少不能少于25个字",
		"name.min" => "模块别名不能少于2个字"
	];
    
 }