<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-18 17:28:49
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-17 09:15:53
 */

namespace Ldy\Traits\Form;

use Closure;

trait Common{


    /**
     * 树 展开已选中的
     *
     * @return $this
     */
    public function defaultExpandedKeys(){
        $this->dynamicAttr(['props.defaultExpandedKeys'=>"menu"]);
        return $this;
    }
    /**
     * 前端通过列表数据更新元素rule规则
     *
     * @param Array $arr ['rule字段名'=>'数据字段名']
     * @return $this
     */
    public function dynamicAttr(Array $arr){
        $this->__updateRule(["dynamicAttr"=>$arr]);
        return $this;
    }
    /**
     * 表单联动控制
     * $form->radio('type',"联动测试")->options([["value"=>0,"label"=>"单选"],["value"=>1,"label"=>"多选"]])->control(1, function(Form $form){
     *       $form->text('title2','asdfsf');
     *   });
     * @param mixed $showVal 条件值
     * @param Closure $callback
     * @return $this
     */
    public function control($showVal, Closure $callback){
        //保存当前元素指针
        $currFieldName = $this->currFieldName;

        $control = ["value"=>$showVal,"rule"=>[]];
        //闭包执行前的元素配置
        $beforeRule = $this->form['rule'];

        call_user_func($callback, $this);

        if(empty($beforeRule)){
            $res = $this->form['rule'];
        }else{
            //通过差集找出闭包执行后添加的
            $res = array_diff_key($this->form['rule'], $beforeRule);
        }

        $tmpRule = [];
        foreach($res as $key=>$item){
            $tmpRule[] = $item;
            //保存提供给验证生成使用, 验证暂时未做处理
            $this->controlElement[$key] = $item;
            //清除$this->form['rule']中的
            unset($this->form['rule'][$key]);
        }

        $control['rule'] = $tmpRule;

        if(empty($this->form['rule'][$currFieldName]["control"])) $this->form['rule'][$currFieldName]["control"] = [];  

        $this->form['rule'][$currFieldName]["control"][] = $control;

        $this->currFieldName = $currFieldName;

        return $this;
    }

     /**
     * 验证字段数据唯一性
     * @return $this
     */
    public function unique(){
        $this->uniqueFields[] = $this->currFieldName;
        return $this;
    }
    
    /**
     * 编辑状态 禁止编辑
     * 不是form-creat 的属性
     * @return $this
     */
    public function editDisabled(){
        $this->__updateRule(["editDisabled"=>true]);
        return $this;
    }

    /**
     * 编辑状态 隐藏
     * 不是form-creat 的属性
     * @return $this
     */
    public function editHidden(){
        $this->__updateRule(["editHidden"=>true]);
        return $this;
    }
    /**
     * 设置元素options
     *
     * @param Array $options
     * @return $this
     */
    public function options(Array $options){
        $type = $this->__getCurrRule('type');
        if($type == 'select'){
            $res = [];
            if(is_array($options) && count($options) == count($options,1)){
                foreach($options as $k => $v) $res[] = ['value'=>$k, 'label' => $v];
            }
            $this->__updateRule(["options"=>empty($res) ? $options:$res]);
        }else if($type == 'cascader'){
            $this->props(["options"=>$options]);
        }else{
            $this->__updateRule(["options"=>$options]);
        }

        return $this;
    }
    /**
     * 增加验证
     *
     * @param Array $arr 一条验证规则
     * @return $this
     */
    public function addValidate(Array $arr){
        $this->form['rule'][$this->currFieldName]['validate'][] = $arr;
        return $this;
    }
   
    /**
     * rule单个元素所有设置
     *
     * @param array $rule
     * @return $this
     */
    public function setRule(Array $rule = []){
        $this->__updateRule($rule);
        return $this;
    }
    /**
     * 设置默认值
     *
     * @param  $val
     * @return $this
     */
    public function value($val = ''){
        if(!empty($this->resources) && isset($this->resources[$this->currFieldName])) $val = $this->resources[$this->currFieldName];
        $this->form['rule'][$this->currFieldName]['value'] = $val;
        return $this;
    }

   /**
    * 设置必填项
    *
    * @return $this
    */
    public function required(){
        $this->__updateRule(['effect'=>['required' =>true]]);
        return $this;
    }

    /**
     * 设置元素属性
     *
     * @param array $props
     * 
     * @return $this
     */
    public function props(Array $props = []){

        // if($this->__getCurrRule('type') == 'cascader') $props = ['props'=>$props];

        $this->__updateRule(["props"=>$props]);
        return $this;
    }

    /**
     * 提示信息
     *
     * @param String $msg
     * @return $this
     */
    public function info(String $msg){
        $this->__updateRule(["info"=>$msg]);
        return $this;
    }

    public function placeholder(String $str){
        $this->props(['placeholder'=>$str]);
        return $this;
    }

    /**
     * 可清除
     *
     * @return $this
     */
    public function clearable(){
        $this->props(["clearable"=>true]);
        return $this;
    }

    /**
     * 禁用
     *
     * @return $this
     */
    public function disabled(){
        $this->props(['disabled'=> true]);
        return $this;
    }

     /**
     * 多选
     *
     * @return $this
     */
    public function multiple(){
        $type = $this->__getCurrRule('type');
        if($type == 'cascader'){
             $this->__updateRule(["props"=>["props"=>['multiple'=>true]]]);
        }else{
            $this->props(['multiple'=>true]);
        }
        return $this;
    }

    /**
     * 单选任意层级
     *
     * @return $this
     */
    public function checkStrictly(){
        $type = $this->__getCurrRule('type');
        if($type == 'cascader'){
            $this->__updateRule(["props"=>["props"=>["checkStrictly"=>true]]]);
        }else{
            $this->props(['checkStrictly'=>true]);
        }
        return $this;
    }

}