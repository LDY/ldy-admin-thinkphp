<?php
/*
 * @Date: 2022-09-23 13:40:40
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-21 23:47:09
 */

namespace Ldy\Traits\Form\Element;

trait UploadFiles{
    
    /**
     * 上传文件类型，默认为图片
     *
     * @return $this
     */
    public function updateFile(){
        $this->props(["uploadType"=>'file']);
        return $this;
    }

    /**
     * 限制上传多少张图片
     *
     * @param Int $num
     * @return $this
     */
    public function limit(Int $num){
        $this->props(["limit"=>$num]);
        return $this;
    }

    /**
     * 上传到那个文件夹
     *
     * @param String $folderName
     * @return $this
     */
    public function saveToFolder(String $folderName){
        return $this->props(["headers"=>["Save-Folder"=>$folderName]]);
    }

    /**
     * 设置上传选择框显示的文件类型 image/* , video/* 参考html 表单规则
     *
     * @param [type] $type
     * @return $this
     */
    public function setAccept($type){
        return $this->props(["accept"=>$type]);
    }
}