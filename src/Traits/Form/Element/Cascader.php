<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-04 17:05:21
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-09-19 13:42:54
 */

namespace Ldy\Traits\Form\Element;

trait Cascader{

    /**
     * 输入框中不显示选中值的完整路径
     *
     * @return $this
     */
    public function notShowAllLevels(){
        $this->__updateRule(["props"=>["showAllLevels"=>false]]);
        return $this;
    }


    /**
     * 多选任意选择层级
     *
     * @return $this
     */
    public function multipleCheckStrictly(){
        $this->__updateRule(["props"=>["props"=>["multiple"=>true,"checkStrictly"=>true]]]);
        return $this;
    }

    /**
     * 次级经过展开方式
     *
     * @return $this
     */
    public function hoverExpand(){
        $this->__updateRule(['props'=>['props'=>["expandTrigger"=>'hover']]]);
        return $this;
    }

    public function emitPath(){
        $this->__updateRule(["props"=>["props"=>["emitPath"=>false]]]);
        return $this;
    }
}