<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-17 09:03:33
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-23 13:30:50
 */

namespace Ldy\Traits\Form\Element;

trait Textarea{

    public function textareaRows(Int $num){
        $this->__updateRule(["props"=>["rows"=>$num]]);
        return $this;
    }
}