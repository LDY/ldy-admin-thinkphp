<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-04 17:41:51
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-09-04 17:44:35
 */

namespace Ldy\Traits\Form\Element;

trait InputNumber{
    
    /**
     * 按钮位置在右边
     *
     * @return $this
     */
    public function btnRight(){
        $this->__updateRule(["props"=>["controlsPosition"=>"right"]]);
        return $this;
    }
}