<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-04 14:02:53
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-21 18:49:36
 */

namespace Ldy\Traits\Form;

trait Config{

    /**
     * 设置表单属性
     *
     * @param array $arr 属性数组
     * @param string $key 属性键
     * @return $this
     */
    public function setOption(Array $arr = [], String $key = ''){
        if(empty($key)){
            $this->form["options"] = $this->__merge($this->form["options"], $arr);
        }else{
            $this->form["options"][$key] = $this->__merge($this->form["options"][$key], $arr);
        }
        return $this;
    }

    /**
     * form 设置
     * https://www.form-create.com/v2/element-ui/global.html#option-form
     *
     * @param array $arr
     * @return void
     */
    public function setForm(Array $arr = []){
        return $this->setOption($arr, 'form');
    }

    /**
     * label 位置
     *
     * @param String $position right/left/top
     * @return void
     */
    public function setFormLabelPosition(String $position){
        return $this->setForm(["labelPosition"=>$position]);
    }

    /**
     * 设置label 宽度
     *
     * @param String $width
     * @return void
     */
    public function setFormLabelWidth(String $width){
        return $this->setForm(["labelWidth"=>$width]);
    }

    /**
     * 提交按钮
     *
     * @return $this
     */
    public function submitBtn(){
        $this->form["options"] = $this->__merge($this->form["options"], ['submitBtn'=>true]);
        return $this;
    }

    /**
     * 重置按钮
     *
     * @return $this
     */
    public function resetBtn(){
        $this->form["options"] = $this->__merge($this->form["options"], ['resetBtn'=>true]);
        return $this;
    }
}