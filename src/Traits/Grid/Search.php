<?php
/*
 * 搜索元素
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-23 13:46:19
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-27 00:32:26
 */
namespace Ldy\Traits\Grid;

trait Search{

    protected $queryElement = [];

    protected $rowElementNum = 4;//查询元素，每行多少个元素

    protected $queryElemenWidth= '100%';

    private $currKey = -1;

    
    /**
     * 搜索输入框
     *
     * @param string $label
     * @param string $placeholder
     * @return $this
     */
    public function input($label = '',$placeholder=''){
        //当前列数据
        $column = $this->__getColumn();

        if(empty($option) && $column['display'] && isset($column['display']['data'])) $option = $column['display']['data'];

        $label = $label ?$label:$column['label'];
        $placeholder = $placeholder ? $placeholder:$label;
        
        $item = $this->__searchElement("input",["field"=>$column['prop'], "label"=>$label,'placeholder'=>$label]);

        if(isset($this->get[$column['prop']])) $item['value'] = $this->get[$column['prop']];

        $this->queryElement[] = $item;
        
        return $this;
    }

    /**
     * 月份范围选择器
     *
     * @return $this
     */
    public function monthRange(){

        $this->datePicker('monthrange', 'yyyy-MM');
        
        return $this;
    }

    public function dateRange(){

        $this->datePicker('daterange');
        
        return $this;
    }

    public function monthSelect(){
        $this->datePicker('month', 'yyyy-MM');
        return $this;
    }

    public function dateSelect(){
        $this->datePicker('month', 'yyyy-MM-dd');
        return $this;
    }

    public function datePicker($type = 'date', $format = 'yyyy-MM-dd'){
        $column = $this->__getColumn();

        $item = $this->__searchElement('date-picker', [
            "date_type"=>$type, 
            "field"=>$column['prop'], 
            "label"=>$column['label'],
            'placeholder'=>$column['label'],
            "format" => $format
        ]);

        $this->queryElement[] = $item;

        return $this;
    }
    /**
     * 下拉筛选项
     *
     * @param array $option [值=>名称]
     * @return $this
     */
    public function select(Array $option = []){
        //当前列数据
        $column = $this->__getColumn();

        if(empty($option) && $column['display'] && $column['display']['data']) $option = $column['display']['data'];
        
        $item = $this->__searchElement("select",["option"=> $option, "field"=>$column['prop'], "label"=>$column['label'],'placeholder'=>$column['label']]);

        if(isset($this->get[$column['prop']])) $item['value'] = $this->get[$column['prop']];

        $this->queryElement[] = $item;
        
        return $this;
    }
    
    /**
     * 多字段input搜索
     *
     * @param Array $arr [filed=>字段名,...]
     * @param string $def
     * @return $this
     */
    public function quickSearch(Array $arr, $def = ''){
        $fieds = [["name"=>"__quick_search", "label"=>"搜索全部"]];
        foreach($arr as $key => $val) $fieds[] = ["name"=>$key, "label"=>$val];

        if(empty($def)) $def = '__quick_search';

        $item = $this->__searchElement("quick_search",["field"=>$fieds,"placeholder"=>"请输入内容", "select"=> $def]);

        $this->queryElement[] = $item;

        return $this;
    }

    /**
     * 设置 placeholder
     *
     * @param String $val
     * @return $this
     */
    public function placeholder(String $val){
        $this->queryElement[$this->currKey]['placeholder'] = $val;
        return $this;
    }


    /**
     * 前端不显示查询元素，手动处理查询，接收字段查询数据
     *
     * @return void
     */
    public function mtHandle($data = ''){
        $item = $this->__searchElement("mt_handle", ['data'=>$data]);
        $this->queryElement[] = $item;
        return $this;
    }

    /**
     * 自定义SQL WHERE 数据过滤
     *
     * @param [type] $sign 数组([值=>'运算符'])或闭包(回调参数：$model, $val)
     * @return $this
     */
    public function filter($sign){
        $this->queryElement[$this->currKey]['filter'] = $sign;
        return $this;
    }

    public function showLabel(){
        $this->queryElement[$this->currKey]['showLabel'] = true;
        return $this;
    }

    /**
     * 设置查询元素，每行多少个元素，超过的自动换行显示更多按钮
     *
     * @param integer $num
     * @return $this
     */
    public function setRowElementNum(int $num){
        $this->rowElementNum = $num;
        return $this;
    }

    public function setQueryElemenWidth(string $width){
        $this->queryElemenWidth = $width;
        return $this;
    }

    private function __searchElement(String $type, array $attr = []){
        $item = ["type"=>"", "field"=>'', "label"=>'', "value"=>'', 'showLabel'=>false];

        $item["type"] = $type;

        $res = array_merge($item, $attr);
        //自动补字段名
        if(empty($res['field'])){
            $column = $this->__getColumn();
            $res['field'] = $column['prop'];
        }

        $this->currKey++;
        
        return $res;
    }

}