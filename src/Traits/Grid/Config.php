<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-24 10:04:31
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-15 14:53:04
 */

namespace Ldy\Traits\Grid;

trait Config{

    /**
     * 表属性
     *
     * @var Array
     */
    protected $table = [
        "border"=>true,//是否显示表格边框
        "size"=>"small",//表格大小，medium / small / mini
    ];

    protected $outTree = false;

    protected $noModel = false; //不使用自定义模型
    
    /**
     * 表格属性
     *
     * @param array $attrs
     * @return $this
     */
    public function table(Array $attrs = []){
        $this->table = array_merge($this->table,$attrs);
        return $this;
    }

    /**
     * 表格边框
     *
     * @param Bool $val
     * @return $this
     */
    public function border(Bool $val = true){
        $this->table["border"] = $val;
        return $this;
    }

    /**
     * 表格大小
     * medium / small / mini
     * @param String $val
     * @return $this
     */
    public function size(String $val){
        $this->table["size"] = $val;
        return $this;
    }

    /**
     * 斑马纹
     *
     * @param boolean $val
     * @return void
     */
    public function stripe(Bool $val= false){
        $this->table["stripe"] = $val;
        return $this;
    }

    // public function pageSize(Int $val = 20){
    //     $this->page['page_size'] = $val;
    //     return $this;
    // }

    public function outTree(){
        $this->outTree = true;
        return $this;
    }

    public function hidePage(){
        $this->table['hidePage'] = true;
        return $this;
    }

    /**
     * 不使用自定义的数据模型过滤
     *
     * @param boolean $val
     * @return $this
     */
    public function noModel($val = true){
        $this->noModel = $val;
        return $this;
    }
}