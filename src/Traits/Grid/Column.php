<?php
/*
 * 列数据元素
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-23 13:36:26
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-25 11:41:57
 */

namespace Ldy\Traits\Grid;

trait Column{

    protected $column = [];

    protected $currColumn = '';

    /**
     * 字段
     *
     * @param String $fieldName
     * @param String $title
     * @return $this
     */
    public function column(String $fieldName, String $label){
        $attr = ["label"=> $label, "prop" => $fieldName, "sortable"=>false, "display"=>["type"=>"text"]];
        $this->__addColumn($fieldName, $attr);

        return $this;
    }

    /**
     * 列表值前缀文本
     *
     * @param string $text
     * @return $this
     */
    public function prefixText(String $text){
        $this->__setColumn("display.prefix", $text);
        return $this;
    }

    /**
     * 列表值后缀文本
     *
     * @param String $text
     * @return $this
     */
    public function suffixText(String $text){
        $this->__setColumn("display.suffix", $text);
        return $this;
    }
    /**
     * 不显示在列表中,但接口有字段数据
     *
     * @return $this
     */
    public function hide(){
        $this->__setColumn("hide", true);
        return $this;
    }

    /**
     * 设置组件属性
     *
     * @param Array $attrs
     * @return $this
     */
    public function attrs(Array $attrs){
        $this->__setColumn('display.attrs', $attrs);
        return $this;
    }

    /**
     * Undocumented function
     *
     * @param string $trigger  click/focus/hover/manual
     * @param string $position top/top-start/top-end/bottom/bottom-start/bottom-end/left/left-start/left-end/right/right-start/right-end
     * @param string $title
     * @return void
     */
    public function popover($trigger = 'hover', $position="top", $title=''){

        $column = $this->__getColumn();
        $title = $title ? $title:$column['label'];
        $this->__setColumnDisplay('popover',["trigger"=>$trigger, "placement"=>$position, 'title'=>$title]);
        return $this;
    }

    public function avatar($size = 50, $url= ''){
        $this->__setColumnDisplay('avatar', ["size"=>$size, "url"=>$url]);
        return $this;
    }
    /**
     * 显示图片
     *
     * @param String $url
     * @param integer $width
     * @param integer $height
     * @return $this
     */
    public function image(Int $width = 50, Int $height = 50, String $url=''){
        $this->__setColumnDisplay('image', ["width"=>$width, "height"=>$height,"url"=>$url]);
        return $this;
    }
    /**
     * 标签
     * 可以通过 attrs 方法设置组件的属性 详情参考 https://element.eleme.cn/#/zh-CN/component/tag
     * 
     * @param String|Array $val 设置组件类型 success/info/warning/danger， 可以是数组，key为数据的值
     * @param String $sign 数据拆分符号，多标签使用
     * @return $this
     */
    public function tag($val, String $sign = ''){
        $this->__setColumnDisplay("tag", $val);
        if(!empty($sign)) $this->__setColumn('display.sign', $sign);

        return $this;
    }
    /**
     * 内容映射
     *
     * @param Array $arr 一维数组|二维数据
     * [1=>'开启',0=>'关闭']
     * [1=>['label'=>'开启','type'=>'success/info/warning/danger'], 0=>[...]]
     * @return $this
     */
    public function using(Array $arr){
        $this->__setColumnDisplay("using", $arr, false);
        return $this;
    }

    /**
     * 支持的组件 using
     * 
     * @param Array $arr [val=>'success/info/warning/danger']
     * @return $this
     */
    public function dot(Array $arr){
        $this->__setColumn('display.dot', $arr);
        return $this;
    }
    /**
     * 前端处理类型
     * default  默认原样输出
     * image    图片输出
     * switch   开关样式输出
     * @param String $val
     * @return $this
     */
    // public function type(String $val="default"){
    //     $this->__setColumn('type', $val);
    //     return $this;
    // }

    /**
     * 列表排序
     * @param mixed $val true=>前端显示列排序, false, 'custom'=>远程排序
     * @return $this
     */
    public function sortable($val = 'custom'){
        $this->__setColumn('sortable', $val);
        return $this;
    }
    /**
     * 列宽
     *
     * @param string $val
     * @return $this
     */
    public function width(string $val = '200'){
        $this->__setColumn('width', $val);
        return $this;
    }

    /**
     * 最小宽度
     *
     * @param string $val
     * @return $this
     */
    public function minWidth(string $val){
        $this->__setColumn('min-width', $val);
        return $this;
    }

    /**
     * 列文字对齐方式
     *
     * @param string $val
     * @return $this
     */
    public function align(String $val = "left"){
        $this->__setColumn('align', $val); //left/center/right
        return $this;
    }

    /**
     * 表头文字对齐方式
     *
     * @param string $val
     * @return $this
     */
    public function headerAlign(String $val = "left"){
        $this->__setColumn('header-align', $val); //left/center/right
        return $this;
    }


    /**
     * 添加列
     *
     * @param String $fieldName
     * @param array $attr
     * @return void
     */
    protected function __addColumn(String $fieldName, Array $attr = []){

        $this->currColumn = $fieldName;

        $this->column[$fieldName] = $attr;
    }


    /**
     * 获取当前字段配置
     *
     * @param string $key
     * @return void
     */
    protected function __getColumn(string $key = ''){
        return empty($key) ? $this->column[$this->currColumn]:$this->column[$this->currColumn][$key];
    }
    /**
     * 设置列属性
     *
     * @param string $key
     * @param [type] $val
     * @return void
     */
    private function __setColumn(string $key, $val){
        $tmp = explode('.', $key);
        if(count($tmp) > 1 && isset($this->column[$this->currColumn][$tmp[0]])){
            //支持 var.var 模式
            $this->column[$this->currColumn][$tmp[0]][$tmp[1]] = $val;
        }else{
            $this->column[$this->currColumn][$key] = $val;
        }
    }
    

    /**
     * 设置前端显示方式
     *
     * @param String $type
     * @param $data
     * @param Bloot $toOneArr 默认二维转一维
     * @return void
     */
    private function __setColumnDisplay(String $type, $data=[], $toOneArr = true){

        $res = ["type"=>$type];
        
        if(!empty($data)){
            $isTwoArr = false;
            if(is_array($data) && count($data) != count($data,1)) $isTwoArr = true;

            if($isTwoArr && $toOneArr){
                //二维转一维
                foreach($data as $key=>$arr) $res[$key] = $arr;
            }else{
                $res['data'] = $data;
            }
        }
        $this->__setColumn("display", $res);
    }

   
}