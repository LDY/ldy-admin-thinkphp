<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-22 22:59:21
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-16 16:00:30
 */

namespace Ldy\Controller;


use Ldy\Controller\BaseAdmin;
use Ldy\Lib\Form;
use Ldy\Lib\Grid;
use Ldy\Models\SysAdmin;
use Ldy\Models\SysAdminRole;
use Ldy\Models\SysDepartment;
use Ldy\Models\SysRole;
use think\Request;
use Ldy\Lib\Show;

class AdminManage extends BaseAdmin{

    protected $title = "管理员";

    private $status = [1=>['label'=>'启用','type'=>'success'],0=>['label'=>'停用','type'=>'danger']];

    protected function grid(){
        $grid = new Grid(new SysAdmin());
        
        $depId = request()->get('department_id');

        $grid->model()->alias('a')
        ->field('a.id,a.avatar,a.name,a.nickname,a.status,a.department_id,GROUP_CONCAT(r.role_id) AS roles')
        ->leftJoin('sys_admin_role r','r.admin_id=a.id')
        ->where('a.name','<>', root_name())
        ->group('a.id');

        if(!empty($depId) && $depId != 'ALL'){
            $depIds = SysDepartment::getSonDepIds($depId, false);
            $grid->model()->where('a.department_id', 'in',$depIds);
        }

        $grid->column('id','ID');
        $grid->column('avatar','头像')->avatar(30);
        $grid->column('name','登录账户');

        $grid->column('nickname','姓名');
        $grid->column('status', '状态')->using($this->status);
        $grid->column('roles','所属角色')->tag(SysRole::getOptions());
        $grid->column('department_id','所属部门')->using(SysDepartment::getOptions());

        return $grid;
    }

    protected function detail($id){
        $model = new SysAdmin();

        $item = $model->alias('a')
                ->field('a.id,a.avatar,a.name,a.nickname,a.status,a.department_id,GROUP_CONCAT(r.role_id) AS roles')
                ->leftJoin('sys_admin_role r','r.admin_id=a.id')
                ->group('a.id')->find($id);

        $show = new Show($item);

        // $show->field("id", "ID");
        $show->field('avatar','头像')->avatar(30);
        $show->field("name","登录账户");
        $show->field("nickname", '姓名');
        $show->field("status","状态")->using($this->status);
        $show->field('department_id','所属部门')->using(SysDepartment::getOptions());
        $show->field('roles','所属角色')->tag(SysRole::getOptions());

        return $show;

    }

    protected function form(){
        $form = new Form(new SysAdmin(),function($model, $id){
            return $model->alias('a')
            ->field('a.id,a.avatar,a.name,a.nickname,a.status,a.department_id,GROUP_CONCAT(r.role_id) AS roles')
            ->leftJoin('sys_admin_role r','r.admin_id=a.id')
            ->group('a.id')->find($id);
        });

        $form->setOption(["labelPosition"=>"left"], "form");

        $form->upload('avatar','头像');

        $form->text("name","登录账户")
        ->addValidate(["required" => true, "message"=>"账户不能为空！"])
        ->addValidate(["min"=> 3, "message"=>'不能少于6个字符'])
        ->addValidate(["max"=> 20, "message"=>"不能大于20个文字"])->unique()->editDisabled();

        $form->text('nickname','姓名')
        ->addValidate(["required" => true, "message"=>"姓名不能为空！"])
        ->addValidate(["min"=> 2, "message"=>'不能少于2个字符'])
        ->addValidate(["max"=> 10, "message"=>"不能大于10个文字"]);

        $form->password("password", "登录密码")
        ->addValidate(["required" => true, "message"=>"登录密码不能为空！"])
        ->addValidate(["min"=> 6, "message"=>'不能少于6个字符'])
        ->addValidate(["max"=> 20, "message"=>"不能大于20个文字"])->editDisabled();
        
        $form->switch('status', '是否启用')->value(1);
        $trees = (new SysDepartment())->cascaderOptionWhere(['status'=>1])->cascaderOption();
        $form->cascader('department_id','所属部门')->options($trees)->required()->checkStrictly()->emitPath();
        $form->select('roles','所属角色')->options(SysRole::getOptions())->required()->multiple();

        return $form;
    }
    

    /**
     * 删除管理员
     * {"DELETE":"删除{title}"}
     * @param Request $request
     * @param Int $id
     * @return void
     */
    public function destroy(Request $request, Int $id){
        if(empty($id)) return $this->errorJson('非法操作！');
        SysAdmin::destroy($id);
        SysAdminRole::where('admin_id', $id)->delete();
        return $this->successJson($id,'删除成功！');
    }
}