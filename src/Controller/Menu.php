<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-16 08:26:01
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2023-01-29 15:57:15
 */

namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;
use Ldy\Lib\Form;
use Ldy\Models\SysMenu;
use Ldy\Models\SysApi;
use Ldy\Models\SysMenuApi;
use think\Request;

class Menu extends BaseAdmin{

    protected $title = "菜单管理";

    protected $logs = [
        'index' => [
            'GET' =>'访问菜单管理列表'
        ],
        'destroy' => [
            'DELETE' => '删除菜单'
        ],
        'store' => [
            'GET' => '新建菜单'
        ],
        'update' => [
            'PUT' => '修改菜单'
        ]
    ];

    public function index(){
        $menus = new SysMenu();
        $trees = $menus->trees();
        return $this->successJson($trees);
    }

    protected function form(){

        $form = new Form(new SysMenu());
        
        $form->rows()->col(function(Form $form){

            $model = $form->model();
            $allDept = $model->cascaderOption('id', 'meta.title');

            // $form->setColAttr(['props'=>['span'=>8]]);

            $form->setOption(["labelWidth"=>"86px","labelPosition"=>"left"], 'form')->submitBtn()->resetBtn();
            // $form->hidden('id','ID');
            $form->text('name','别名')->placeholder('系统唯一且与内置组件名一致，否则导致缓存失效。')->clearable();
            $form->cascader('parent_id', '上级菜单')->options($allDept)->checkStrictly()->disabled()->emitPath();

            $form->object('meta', function(Form $form){
                $form->text('title','显示名称')->clearable();
                $form->radio('type', '类型')->options([["value"=>'menu',"label"=>'菜单'],["value"=>'button',"label"=>'按钮']])->props(['type'=>'button'])->value('menu');
                $form->icons('icon','菜单图标');
                $form->checkbox('hidden','是否隐藏')->options([
                    ["value"=>"menu","label"=>"隐藏菜单","disabled"=>false],
                    ["value"=>"breadcrumb","label"=>"隐藏面包屑","disabled"=>false],
                ]);
                // $form->cascader('refresh', '自动')
            });

            $form->text('path','路由地址');
            $form->text('redirect','重定向');
            $form->text('component','视图')->placeholder('通常不用填写，系统根据路由路径自动绑定');
            $form->number('rand', '排序')->value(1)->btnRight();
            $form->switch('is_public','通用菜单')->options([0=>'否',1=>'是'])->info('通用菜单，是指所有角色都需要有的系统基础菜单，创建角色时默认选中！');

            
        })->col(function(Form $form){
            $form->setColAttr(['span'=>1]);
            $form->divider()->props(['direction'=>'vertical']);
        })->col(function(Form $form){
            $form->setColAttr(['span'=>11]);
            $apiTrees = (new SysApi())->cascaderOption();
            $form->cascader('apis','接口权限')->options($apiTrees)->multipleCheckStrictly()->emitPath()->clearable();
        });

        return $form;
    }

    /**
     * 删除菜单
     * {"DELETE":"删除菜单"}
     * @param Request $request
     * @param Int $id
     * @return void
     */
    public function destroy(Request $request, Int $id){
        $model = SysMenu::where('parent_id', $id)->select();
        if(!$model->isEmpty()) return $this->errorJson('存在子菜单，请先删除该菜单下面的所有子菜单！');
        SysMenu::destroy($id);
        SysMenuApi::where('menu_id', $id)->delete();
        return $this->successJson($id,'删除成功！');
    }
}