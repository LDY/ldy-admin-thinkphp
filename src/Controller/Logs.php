<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-08 16:40:42
 */
namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;
use think\Request;
use Ldy\Models\SysLogs;
use Ldy\Models\SysAdmin;
use Ldy\Lib\Show;
use Ldy\Lib\Grid;


class Logs extends BaseAdmin{

    protected $isRecordLogs = false;

    protected function grid(){
        $grid = new Grid(new SysLogs());

        $grid->model()->order('create_time','desc');
        
        $grid->quickSearch(['id'=>"ID",'name'=>'用户名']);

        $grid->column("id","#")->width('100');
        $grid->column("admin_id","管理员ID")->select(SysAdmin::getOptions());
        $grid->column("name","用户名");
        $grid->column("title","日志名");
        
        $grid->column("url","请求接口");
        $grid->column("code","响应状态");
        $grid->column("method","请求类型");
        

        $grid->column("ip","客户端IP");

        $grid->column("create_time","日志时间");

        return $grid;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function detail($id){
        $show = new Show(SysLogs::find($id));

        $show->field("id", "ID");
        $show->field("name","用户名");
        $show->field("title", '日志名');
        $show->field("url","请求接口");
        $show->field('code', '状态');
        

        $show->field("ip","客户端IP");

        $show->field("create_time","日志时间");

        $show->field('data', '请求数据');


        return $show;

    }

    // protected function form(){
    //     $form = new Form(new SysLogs());

    //     return $form;
    // }
}