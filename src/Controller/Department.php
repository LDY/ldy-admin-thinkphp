<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-03 15:34:20
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-07-26 12:13:05
 */

namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;
use Ldy\Lib\Grid;
use Ldy\Lib\Form;
use Ldy\Models\SysDepartment;
use think\Request;

class Department extends BaseAdmin{

    protected $title = "单位管理";


    /**
     * 列表
     */
    protected function grid(){

        $grid = new Grid(new SysDepartment());

        $grid->border(false)->outTree()->hidePage();
        // $grid->quickSearch(['id'=>"ID",'label'=>'模块名称']);

        // $grid->column("id", 'Id')->width(100);
        $grid->column("name","单位名称");
        $grid->column("rand", "排序");
        $grid->column("status", "是否启用")->using([1=>['label'=>'启用','type'=>'success'],0=>['label'=>'停用','type'=>'danger']])->select([1=>'启用',0=>'停用']);
        $grid->column('bz', '备注');
        $grid->column('create_time', '创建时间');
        // print_r();

        return $grid;
    }

    /**
     * 创建表单
     */
    protected function form(){
        $form = new Form(new SysDepartment());
        
        $model = $form->model();

        $allDept = $model->cascaderOption();

        $form->cascader('parent_id', '上级单位')->options($allDept)->checkStrictly()->emitPath()->clearable();

        $form->text("name", '单位名称')->required();

        $form->switch('status', '是否启用')->value(1);
        
        $form->number("rand", "排序")->value(1)->btnRight();

        $form->textarea('bz', "备注");


        return $form;
    }

    /**
     * {"DELETE":"删除部门"}
     * @param Request $request
     * @param Int $id
     * @return void
     */
    public function destroy(Request $request, Int $id){
        $model = SysDepartment::where('parent_id', $id)->select();
        if(!$model->isEmpty()) return $this->errorJson('存在子单位，请先删除该部门下面的所有子单位！');
        SysDepartment::destroy($id);
        return $this->successJson($id,'删除成功！');
    }
}