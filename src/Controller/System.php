<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-13 15:51:10
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-09-26 12:58:04
 */
namespace Ldy\Controller;

use Ldy\controller\BaseAdmin;
use think\Request;
use Ldy\Models\SysMenu;

class System extends BaseAdmin{

  public function getTableFields(Request $request){
    $table = $request->get('table');
    
    $class = "\\Ldy\\Models\\{$table}";

    if(empty($table) || !class_exists($class)) return $this->errorJson();
  

    $fields = (new $class)->getTableFieldsList();

    return $this->successJson($fields);

  }

  public function token(){
    $str = '{"code":200,"data":{"token":"SCUI.Administrator.Auth","userInfo":{"userId":"1","userName":"Administrator","dashboard":"0","role":["SA","admin","Auditor"]}},"message":""}';
    $data = json_decode($str, true);

    return json($data);
  }


  // public function menu(){
  //   return json(["code"=> 200 "data"=>['path'=> '/abc', "component"=>'test2/index']]);
  // }
  public function menuList(){
    $str = '{
      "code": 200,
      "data": [
        {
          "id": "00001",
          "parentId": null,
          "name": "system",
          "path": "/system",
          "meta": {
            "title": "系统",
            "icon": "el-icon-setting",
            "type": "menu"
          },
          "apiList": [
            {
              "code": "system.list",
              "url": "/api/system/list"
            }
          ],
          "component": "",
          "children": [
            {
              "id": "00001001",
              "parentId": "00001",
              "name": "user",
              "path": "/system/user",
              "meta": {
                "title": "用户管理",
                "icon": "el-icon-setting",
                "type": "menu"
              },
              "component": "",
              "children": [
                {
                  "id": "00001001001",
                  "parentId": "00001001",
                  "name": "user.add",
                  "meta": {
                    "title": "新增",
                    "type": "button"
                  }
                },
                {
                  "id": "00001001002",
                  "parentId": "00001001",
                  "name": "user.edit",
                  "meta": {
                    "title": "编辑",
                    "type": "button"
                  }
                },
                {
                  "id": "00001001003",
                  "parentId": "00001001",
                  "name": "user.del",
                  "meta": {
                    "title": "删除",
                    "type": "button"
                  }
                }
              ]
            }
          ]
        },
        {
          "id": "00002",
          "parentId": null,
          "name": "other",
          "path": "/other",
          "meta": {
            "title": "其他",
            "icon": "el-icon-more",
            "type": "menu"
          },
          "component": "",
          "children": [
            {
              "id": "00002001",
              "parentId": "00002",
              "name": "directive",
              "path": "/other/directive",
              "meta": {
                "title": "指令",
                "icon": "el-icon-price-tag",
                "type": "menu"
              },
              "component": "",
              "children": [
                {
                  "id": "00002001001",
                  "parentId": "00002001",
                  "name": "directive.add",
                  "meta": {
                    "title": "新增",
                    "type": "button"
                  }
                },
                {
                  "id": "00002001002",
                  "parentId": "00002001",
                  "name": "directive.edit",
                  "meta": {
                    "title": "编辑",
                    "type": "button"
                  }
                },
                {
                  "id": "00002001003",
                  "parentId": "00002001",
                  "name": "directive.del",
                  "meta": {
                    "title": "删除",
                    "type": "button"
                  }
                }
              ]
            }
          ]
        }
      ],
      "message": ""
    }';

    $data = json_decode($str, true);

    return json($data);
  }

  public function myMenu(){
    $model = new SysMenu();
    $menu = $model->trees();
    return $this->successJson(["menu"=>$menu]);
  }
  public function myMenu2(){
        $str = '{
            "code": 200,
            "data": {
              "menu": [
                {
                  "name": "home",
                  "path": "/home",
                  "meta": {
                    "title": "首页",
                    "icon": "el-icon-eleme-filled",
                    "type": "menu"
                  },
                  "children": [
                    {
                      "name": "dashboard",
                      "path": "/dashboard",
                      "meta": {
                        "title": "控制台",
                        "icon": "el-icon-menu",
                        "affix": true,
                        "keepAlive":true,
                        "type":"menu"
                      },
                      "component": "home"
                    },
                    {
                      "name": "userCenter",
                      "path": "/user",
                      "meta": {
                        "title": "帐号信息",
                        "icon": "el-icon-user"
                      },
                      "component": "user"
                    }
                  ]
                },
                {
                  "name": "system",
                  "path": "/system",
                  "meta": {
                    "title": "系统",
                    "icon": "el-icon-setting",
                    "type": "menu"
                  },
                  "children": [
                    {
                      "path": "/system/setting",
                      "name": "SystemSetting",
                      "meta": {
                        "title": "系统设置",
                        "icon": "el-icon-setting",
                        "type": "menu"
                      },
                      "component": "system/setting"
                    },
					{
					  "path": "",
					  "name": "auth_manage",
					  "meta": {"title":"角色权限","icon": "el-icon-lock","type": "menu"},
					  "children":[
							  {
							  "path": "/setting/user",
							  "name": "user",
							  "meta": {
								"title": "用户管理",
								"icon": "el-icon-user",
								"type": "menu"
							  },
							  "component": "setting/user"
							},
							{
							  "path": "/setting/role",
							  "name": "role",
							  "meta": {
								"title": "角色管理",
								"icon": "el-icon-postcard",
								"type": "menu"
							  },
							  "component": "setting/role"
							},
							{
							  "path": "/system/department",
							  "name": "systemDepartment",
							  "meta": {
								"title": "部门管理",
								"icon": "qs-icon-bumen",
								"type": "menu"
							  },
							  "component": ""
							},
							 {
							  "path": "/system/menu",
							  "name": "systemMenu",
							  "meta": {
								"title": "菜单管理",
								"icon": "el-icon-menu",
								"type": "menu"
							  },
							  "component": ""
							},
              {
							  "path": "/system/api_manage",
							  "name": "systemApiManage",
							  "meta": {
								"title": "API管理",
								"icon": "el-icon-menu",
								"type": "menu"
							  },
							  "component": ""
							}
					  ]
					},
                   
                    {
                      "path": "/setting/task",
                      "name": "task",
                      "meta": {
                        "title": "计划任务",
                        "icon": "el-icon-alarm-clock",
                        "type": "menu"
                      },
                      "component": "setting/task"
                    },
                    {
                      "path": "/system/modules",
                      "name": "ModulesIndex",
                      "meta": {
                        "title": "模块管理",
                        "icon": "el-icon-help",
                        "type": "menu"
                      },
					  "children":[
						{
						  "path": "/system/modules/create",
						  "name": "moduleCreate",
						  "meta": {
							"title": "创建模块",
							"icon": "el-icon-menu",
							"type": "menu",
							"hidden":true,
							"fullpage":false,
							"router_view":"modules"
						  }
						},
						{
						  "path": "/system/modules/list",
						  "name": "moduleList",
						  "meta": {
							"title": "模块列表",
							"icon": "el-icon-menu",
							"type": "menu",
							"hidden":true,
							"fullpage":false,
							"router_view":"modules"
						  }
						}
					  ]
                    },
                    {
                      "path": "/setting/log",
                      "name": "log",
                      "meta": {
                        "title": "系统日志",
                        "icon": "el-icon-warning-outline",
                        "type": "menu"
                      },
                      "component": "setting/log"
                    }
                  ]
                },
                {
                  "path": "/other/about",
                  "name": "about",
                  "meta": {
                    "title": "关于",
                    "icon": "el-icon-info-filled",
                    "type": "menu"
                  },
                  "component": "other/about"
                }
              ],
              "permissions": [
                "list.add",
                "list.edit",
                "list.delete",
                "user.add",
                "user.edit",
                "user.delete"
              ]
            },
            "message": ""
          }';

          $data = json_decode($str, true);

          return json($data);
  }
}