<?php

namespace Ldy\Facade;

/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-12 15:28:17
 */

 //使用thinkphp 静态代理
use think\Facade;

class ClientConfig extends Facade{

    protected static function getFacadeClass()
    {
        return 'Ldy\Lib\SetClientConfig';
    }
}
